package com.suby.hello;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class HelloController {

    @Value("${config.name}")
    private String message;

    @GetMapping("/")
    @ResponseBody
    String home() {
        return "Hello World! " + this.message;
    }

}
